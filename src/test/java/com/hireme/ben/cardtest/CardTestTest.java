
package com.hireme.ben.cardtest;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by benstpierre on 2017-05-23.
 */
public class CardTestTest {


    @Test
    public void testHandOfCardsToDescriptionMultipleCards() {
        final HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(Card.FourOfSpades);
        handOfCards.addCard(Card.QueenOfHearts);
        Assert.assertEquals("[Four of Spades, Queen of Hearts]", handOfCards.getDescription());
    }

    @Test
    public void testHandOfCardsToDescriptionOneCard() {
        final HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(Card.AceOfDiamonds);
        Assert.assertEquals("[Ace of Diamonds]", handOfCards.getDescription());
    }


    @Test(expected = IllegalArgumentException.class)
    public void testHandOfCardsToDescriptionNoCards() {
        new HandOfCards().getDescription();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeckOfCardsDealAllCardsNumHandsTooMany() {
        DeckOfCards.buildDeck().dealAllCards(53, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeckOfCardsDealAllCardsNumHandsZero() {
        DeckOfCards.buildDeck().dealAllCards(0, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeckOfCardsDealAllCardsPerHand0() {
        DeckOfCards.buildDeck().dealAllCards(2, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeckOfCardsDealAllCardsTooManyCardsPerHand() {
        DeckOfCards.buildDeck().dealAllCards(5, 12);
    }

    @Test
    public void testDeckOfCardsDealAllCardsSuccess() {
        final List<HandOfCards> hands = DeckOfCards.buildDeck().dealAllCards(4, 3);
        Assert.assertEquals("Should have 4 hands", 4, hands.size());
        for (HandOfCards handOfCards : hands) {
            Assert.assertEquals("Expected 3 cards per hand", 3, handOfCards.getCards().size());
        }
    }

    @Test
    public void testDealAllCards() {
        final List<HandOfCards> hands = DeckOfCards.buildDeck().dealAllCards(4, 13);
        final Set<Card> allPossibleCards = new HashSet<>(Arrays.asList(Card.values()));
        final Set<Card> cardsWeJustDealt = hands.stream().map(HandOfCards::getCards).flatMap(List::stream).collect(Collectors.toSet());
        Assert.assertEquals("Expected one of each card should hand been dealt", allPossibleCards, cardsWeJustDealt);
    }


}

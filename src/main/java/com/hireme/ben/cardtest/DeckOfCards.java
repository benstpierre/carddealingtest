package com.hireme.ben.cardtest;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by benstpierre on 2017-05-19.
 */
public class DeckOfCards {

    private static final SecureRandom SECURE_RANDOM = new SecureRandom();

    private final List<Card> deckOfCards;

    private DeckOfCards(List<Card> deckOfCards) {
        this.deckOfCards = deckOfCards;
    }

    public List<Card> getDeckOfCards() {
        return deckOfCards;
    }

    public static DeckOfCards buildDeck() {
        final List<Card> cardList = new ArrayList<>();
        Collections.addAll(cardList, Card.values());
        //This is one of those times I could totally show you I know how to build a shuffle function.
        //However reinventing this when it already runs in linear time would be a poor practice.
        Collections.shuffle(cardList, SECURE_RANDOM);

        return new DeckOfCards(cardList);
    }

    public List<HandOfCards> dealAllCards(int numHands, int numCardsPerHand) {
        if(numHands ==0){
            throw new IllegalArgumentException("Cannot deal 0 hands");
        }
        if(numCardsPerHand==0){
            throw new IllegalArgumentException("Cannot deal 0 cards per hand");
        }
        final int maxPerHand = Card.values().length / numHands;
        //Couple of sanity checks
        if (numHands > Card.values().length) {
            throw new IllegalArgumentException("Too many hands");
        }
        if (maxPerHand < numCardsPerHand) {
            throw new IllegalArgumentException("Too many cards per hand");
        }
        final List<HandOfCards> hands = new ArrayList<>(numHands);
        for (int i = 0; i < numHands; i++) {
            final HandOfCards hand = new HandOfCards();
            for (int j = 0; j < numCardsPerHand; j++) {
                hand.addCard(dealCard());
            }
            hands.add(hand);
        }
        return hands;
    }


    public Card dealCard() {
        final Card card = deckOfCards.get(0);
        deckOfCards.remove(0);
        return card;
    }
}

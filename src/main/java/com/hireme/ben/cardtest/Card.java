package com.hireme.ben.cardtest;

/**
 * Created by benstpierre on 2017-05-19.
 */
public enum Card {

    /*
    Java Enums are great for this kind of lookup info.
    If this was internationalized instead of description it would be a property file bundle.
    I could have derived the names via (rank | facename + 'of' + SUIT) but I find manual configs are more flexible for this kind of code
     */

    TwoOfClubs(Suit.Clubs, "Two of Clubs"),
    ThreeOfClubs(Suit.Clubs, "Three of Clubs"),
    FourOfClubs(Suit.Clubs, "Four of Clubs"),
    FiveOfClubs(Suit.Clubs, "Five of Clubs"),
    SixOfClubs(Suit.Clubs, "Six of Clubs"),
    SevenOfClubs(Suit.Clubs, "Seven of Clubs"),
    EightOfClubs(Suit.Clubs, "Eight of Clubs"),
    NineOfClubs(Suit.Clubs, "Nine of Clubs"),
    TenOfClubs(Suit.Clubs, "Ten of Clubs"),
    JackOfClubs(Suit.Clubs, "Jack of Clubs"),
    QueenOfClubs(Suit.Clubs, "Queen of Clubs"),
    KingOfClubs(Suit.Clubs, "King of Clubs"),
    AceOfClubs(Suit.Clubs, "Ace of Clubs"),


    TwoOfDiamonds(Suit.Diamonds, "Two of Diamonds"),
    ThreeOfDiamonds(Suit.Diamonds, "Three of Diamonds"),
    FourOfDiamonds(Suit.Diamonds, "Four of Diamonds"),
    FiveOfDiamonds(Suit.Diamonds, "Five of Diamonds"),
    SixOfDiamonds(Suit.Diamonds, "Six of Diamonds"),
    SevenOfDiamonds(Suit.Diamonds, "Seven of Diamonds"),
    EightOfDiamonds(Suit.Diamonds, "Eight of Diamonds"),
    NineOfDiamonds(Suit.Diamonds, "Nine of Diamonds"),
    TenOfDiamonds(Suit.Diamonds, "Ten of Diamonds"),
    JackOfDiamonds(Suit.Diamonds, "Jack of Diamonds"),
    QueenOfDiamonds(Suit.Diamonds, "Queen of Diamonds"),
    KingOfDiamonds(Suit.Diamonds, "King of Diamonds"),
    AceOfDiamonds(Suit.Diamonds, "Ace of Diamonds"),


    TwoOfSpades(Suit.Spades, "Two of Spades"),
    ThreeOfSpades(Suit.Spades, "Three of Spades"),
    FourOfSpades(Suit.Spades, "Four of Spades"),
    FiveOfSpades(Suit.Spades, "Five of Spades"),
    SixOfSpades(Suit.Spades, "Six of Spades"),
    SevenOfSpades(Suit.Spades, "Seven of Spades"),
    EightOfSpades(Suit.Spades, "Eight of Spades"),
    NineOfSpades(Suit.Spades, "Nine of Spades"),
    TenOfSpades(Suit.Spades, "Ten of Spades"),
    JackOfSpades(Suit.Spades, "Jack of Spades"),
    QueenOfSpades(Suit.Spades, "Queen of Spades"),
    KingOfSpades(Suit.Spades, "King of Spades"),
    AceOfSpades(Suit.Spades, "Ace of Spades"),


    TwoOfHearts(Suit.Hearts, "Two of Hearts"),
    ThreeOfHearts(Suit.Hearts, "Three of Hearts"),
    FourOfHearts(Suit.Hearts, "Four of Hearts"),
    FiveOfHearts(Suit.Hearts, "Five of Hearts"),
    SixOfHearts(Suit.Hearts, "Six of Hearts"),
    SevenOfHearts(Suit.Hearts, "Seven of Hearts"),
    EightOfHearts(Suit.Hearts, "Eight of Hearts"),
    NineOfHearts(Suit.Hearts, "Nine of Hearts"),
    TenOfHearts(Suit.Hearts, "Ten of Hearts"),
    JackOfHearts(Suit.Hearts, "Jack of Hearts"),
    QueenOfHearts(Suit.Hearts, "Queen of Hearts"),
    KingOfHearts(Suit.Hearts, "King of Hearts"),
    AceOfHearts(Suit.Hearts, "Ace of Hearts");


    private final Suit suit;
    private final String description;

    public Suit getSuit() {
        return suit;
    }

    public String getDescription() {
        return description;
    }

    public enum Suit {
        Clubs(Color.Black),
        Diamonds(Color.Red),
        Hearts(Color.Red),
        Spades(Color.Black);


        private final Color color;

        Suit(Color color) {
            this.color = color;
        }

        public Color getColor() {
            return color;
        }
    }

    public enum Color {
        Red,
        Black
    }

    Card(Suit suit, String description) {
        this.suit = suit;
        this.description = description;
    }


}

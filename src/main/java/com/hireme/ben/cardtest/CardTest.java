package com.hireme.ben.cardtest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by benstpierre on 2017-05-19.
 * <p>
 * Provides a basic TUI (text user interface) to validate Ben's code works.
 * If this app were bigger I would have a CardService and inject it into what I am using
 * But for the purpose of a demo terminal app will leave everything in one class.
 * <p>
 * The easiest way to run this is just [mvn clean package exec:java -Dexec.mainClass="com.hireme.ben.cardtest.CardTest"] in the terminal
 * it does a full clean, test/package and run.  It also creates an executable jar
 */
public class CardTest {


    private static final Object LOCK_OBJECT = new Object();

    private DeckOfCards deckOfCards;

    public static void main(String[] args) {
        //Start TUI in a new thread and wait
        synchronized (LOCK_OBJECT) {
            try {
                new CardTest().doMainMenu(true);
                LOCK_OBJECT.wait();
            } catch (InterruptedException ignored) {
                //Do nothing, let the program exit normally.
            }
        }
    }

    /*
    Every time you start at the main menu create a new thread so the previous stack can be GC'ed
    Without this a TUI program that uses recursion like mine can have a stack overflow.
    This is not problematic for a quick test but it's nice to make it impossible to crash the program.
 */
    public void doMainMenu(boolean startNewThread) {
        if (startNewThread) {
            new Thread(() -> doMainMenu(false)).start();
            return;
        }
        this.deckOfCards = DeckOfCards.buildDeck();
        println("Ben's Card Test");
        println("[1] to deal single card");
        println("[2] to deal hands");
        println("[Q] to QUIT");
        final String choice = readLine();
        if ("1".equals(choice)) {
            dealSingleCard(false);
        } else if ("2".equals(choice)) {
            promptNumberOfHands(false);
        } else if ("Q".equalsIgnoreCase(choice)) {
            quitProgram();
        } else {
            println("Invalid Selection");
            doMainMenu(true);
        }
    }

    private void quitProgram() {
        synchronized (LOCK_OBJECT) {
            LOCK_OBJECT.notifyAll();
        }
        System.out.println("Goodbye!");
    }


    private void dealSingleCard(boolean outOfCards) {
        if (outOfCards) {
            println("You are out of cards, returning to main menu.");
            doMainMenu(true);
        } else {
            println("Press Enter to deal card or [M] to return to main menu");
            if ("M".equalsIgnoreCase(readLine())) {
                doMainMenu(true);
            } else {
                final Card singleCard = deckOfCards.dealCard();
                final int cardsLeft = deckOfCards.getDeckOfCards().size();
                final String strCardsLeft = cardsLeft == 0 ? "no" : String.valueOf(cardsLeft);
                println("You drew \"" + singleCard.getDescription() + "\"... " + strCardsLeft + " cards remain.");
                dealSingleCard(cardsLeft == 0);
            }
        }
    }

    /**
     * @param startNewThread Used to avoid a {@link StackOverflowError} if the user endlessly inputs an invalid selection
     */
    private void promptNumberOfHands(boolean startNewThread) {
        if (startNewThread) {
            new Thread(() -> promptNumberOfHands(false)).start();
            return;
        }
        println("How many hands would you like? MAX is 52");
        final String strNumHands = readLine();
        final int numHands;
        try {
            numHands = Integer.valueOf(strNumHands);
        } catch (NumberFormatException e) {
            println("Invalid selection!");
            promptNumberOfHands(true);
            return;
        }
        if (numHands > 52 || numHands < 1) {
            println("Invalid selection!");
            promptNumberOfHands(true);
            return;
        }
        promptCardsPerHand(numHands, false);
    }

    /**
     * @param startNewThread Used to avoid a {@link StackOverflowError} if the user endlessly inputs an invalid selection
     */
    private void promptCardsPerHand(int numHands, boolean startNewThread) {
        if (startNewThread) {
            new Thread(() -> promptCardsPerHand(numHands, false)).start();
            return;
        }
        final int maxPerHand = Card.values().length / numHands;
        println("How many cards per hand?  MAX is [" + maxPerHand + "]");
        final String strNumCardsPerHand = readLine();
        final int numCardsPerHand;
        try {
            numCardsPerHand = Integer.valueOf(strNumCardsPerHand);
        } catch (NumberFormatException e) {
            println("Invalid selection!");
            promptCardsPerHand(numHands, true);
            return;
        }
        if (numCardsPerHand > maxPerHand || numCardsPerHand < 1) {
            println("Invalid selection!");
            promptCardsPerHand(numHands, true);
        } else {
            dealCards(numHands, numCardsPerHand);
        }
    }

    private void dealCards(int numHands, int numCardsPerHand) {
        println("Dealing cards");
        final List<HandOfCards> hands = this.deckOfCards.dealAllCards(numHands, numCardsPerHand);
        println("Cards dealt as follows:");
        for (int i = 0; i < hands.size(); i++) {
            final String strDesc = String.format("Hand %d of %d=%s", i + 1, hands.size(), hands.get(i).getDescription());
            println(strDesc);
        }
        println("Press [Enter] to return to main menu");
        readLine();
        doMainMenu(true);
    }

    //Useful to have read/print commands wrapped in case we need to change how they are implemented
    //Would help with mocking if needed for tests
    private String readLine() {
        try {
            final BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
            return buffer.readLine();
        } catch (IOException e) {
            //This should never happen given a terminal window environment
            throw new RuntimeException(e);
        }
    }

    private void println(String message) {
        System.out.println(message);
    }


}

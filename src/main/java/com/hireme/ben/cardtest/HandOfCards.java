package com.hireme.ben.cardtest;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * Created by benstpierre on 2017-05-19.
 */
public class HandOfCards {


    private final List<Card> cards = new ArrayList<>();


    public void addCard(Card card) {
        this.cards.add(card);
    }

    public List<Card> getCards() {
        return cards;
    }


    public String getDescription() {
        if (cards.isEmpty()) {
            throw new IllegalArgumentException("No cards added!");
        }
        //Love the new Java 8 apis that were inspired by the older Google Guava releases
        final StringJoiner sj = new StringJoiner(", ", "[", "]");
        for (Card card : cards) {
            sj.add(card.getDescription());
        }
        return sj.toString();
    }
}
